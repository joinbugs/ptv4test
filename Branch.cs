﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ptv4Test
{
    public class Branch
    {
        public List<Branch> Branches { get; set; }
        public string Index { get; set; }
        public string Text { get; set; }
    }
}