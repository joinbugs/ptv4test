﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Ptv4Test
{
    class Program
    {
        static void PrintTree(Branch tree)
        {
            Console.WriteLine(tree.Text);

            for(int i = 0; i < tree.Branches.Count; i++)
            {
                PrintTree(tree.Branches[i]);
            }
        }

        static void Main(string[] args)
        {
            string workingDirectory = Environment.CurrentDirectory;
            string pathTree = Path.Combine(Directory.GetParent(workingDirectory).Parent.Parent.FullName, @"Assets\input.txt");

            List<Branch> tree = new List<Branch>();

            foreach (string rawBranch in File.ReadLines(pathTree))
            {
                Branch branch = new Branch();
                branch.Index = rawBranch.Split(",")[0].Replace(".", string.Empty);
                branch.Text = rawBranch;

                tree.Add(branch);
            }            

            for(int i = 0; i < tree.Count; i++)
            {
                List<Branch> branches = new List<Branch>();
                for(int j = 0; j < tree.Count; j++)
                {
                    if((tree[i].Index.Length + 1) == tree[j].Index.Length &&
                        tree[i].Index == tree[j].Index.Substring(0, tree[j].Index.Length - 1))
                    {
                        branches.Add(tree[j]);
                    }
                }
                tree[i].Branches = branches;
            }

            while(true)
            {
                Console.Write("Enter index > ");
                string index = Console.ReadLine().Replace(".", string.Empty);

                Console.WriteLine();
                Branch subTree = tree.FirstOrDefault(e => e.Index == index);

                PrintTree(subTree);
                Console.WriteLine();
            }
        }
    }
}
